(function($){
	$.fn.testimonials = function( options ) {

	     // Settings
	    var plugin = this,
	        defaults = {
	        	json: {},
	            interval: 2000,
	            //onAfterShow: function( data ){ plugin.onAfterShow( data ); }
	        };
	   
	    // Init
	    plugin.params = $.extend({}, defaults, options);
	    //plugin.$ = plugin.params.element;

	    plugin.init = function(){
	    	var json = this.params.json;
				
	    	//if( json.length ){
	    		plugin.create_html(json);
	    	//}
	    };

	    plugin.bind_plugin_events = function(){
    		plugin.slideshow();

    		var $slide = $('.slide');

    		$slide.on('click', plugin.click_slide);
	    };

	    plugin.create_html = function(json){
    		var html = plugin.loop_json(json);

    		$(plugin).html(html).find('.slide:first-child').addClass('current');
    		plugin.bind_plugin_events();
    		return true;
	    };

	    plugin.loop_json = function(json){
	    	var html = '',
	    		i = 0;
	    			
	    	for (var prop in json) {
	    		if( $.isArray(json[prop]) ){
	    			var inner_json = json[prop];
	    			for (var inner_prop in inner_json) {
	    				html += this.create_slide(inner_json[inner_prop]);
	    				i++;
	    			}
	    			break;
	    		}

			    if (json.hasOwnProperty(prop)) {
			    	html += this.create_slide(json[prop]);
    				i++;
			    }
			}

			return html;
	    };

	    plugin.create_slide = function(slide){
	    	var html,
	    		inner = '',
	    		slide = slide;
		
    		inner = plugin.html_factory('h2', slide.title);
    		inner += plugin.html_factory('p', slide.content);
    		inner += plugin.html_factory('span', slide.by);
    		html = plugin.html_factory('div', inner, 
    			{'class': 'slide', 'rel': slide.id});

    		return html;
	    };

	    plugin.html_factory = function(tag, content, attrs){
	    	attrs = plugin.parse_attrs(attrs);

	    	return '<' +tag+ ' ' +attrs+ '>' +content+ '</' +tag+ '>';
	    };

	    plugin.parse_attrs = function(attrs){
	    	var output = [];

	    	for (var prop in attrs) {
			    if (attrs.hasOwnProperty(prop)) {
			    	var attr = plugin.create_attr(prop, attrs[prop]);
			    	output.push(attr);
			    }
			}
			return output.join(' ');
	    };

	    plugin.create_attr = function(attr, value){
	    	return attr + '="' +value+ '"';
	    };

	    plugin.slideshow = function(){
	    	var interval = plugin.params.interval;

	    	self.setInterval(
	    		function(){
	    			plugin.swap_slide();
	    		}
    		,interval);
	    };

	    plugin.swap_slide = function(){
	    	var $slide = $('.slide'),
	    		$current = $('.current'),
	    		$next = $current.next();
		
    		$current.removeClass('current');
    		
	    	if( $next.length == 0 ) {
		        $next = $slide.first();
    			$next.remove().appendTo( $(plugin) );
		    }


    		$next.addClass('current');

    		return false;
	    };

	    plugin.click_slide = function(e){
	    	var $element = $(e.target),
	    		$wrapper = $('#testimonials');

    		if( !$wrapper.hasClass('expand') ){
    			var $body = $('body'),
    				$overlay = $('<div class="overlay"></div>');

    			$wrapper.toggleClass('expand');
    			$body.append( $overlay );
    		}
    		return false;
	    };

	    plugin.onAfterShow = function( data ){

	        // Alowing setting the callback function in jQuery style
	        if( $.isFunction( data ) ){ plugin.params.onAfterShow = data; return; }

	        // Default
	        console.log('Default event');

	    }

	    plugin.init();

	};
}(jQuery));